
var Health;
var Score;
var Ultimate;
var Pause;
var pauseLabel;
var bossdisplay = false;
var bossAttack = false;
var bossCount = 0;
var bossKill = 0;
var ultimateSkills;
var itemdisplay;
var enhanceMode;
var enemyIndex;
var volumeNum;

var playState = {
    preload: function() {}, // The proload state does nothing now. 
    create: function() {
        //border
        game.world.setBounds(0, 0, 1280, 860);
        //map
        this.map = game.add.tileSprite(0, 0, 1280, 1558, 'map');
        //初始值
        game.global.score = 0;
        game.global.health = 20;
        ultimateSkills = 0;
        itemdisplay = 0;
        enhanceMode = 0;
        enemyIndex = 0;
        volumeNum = 1;

        //UI
        Health = game.add.text(1040, 40, 'Health:'+game.global.health, { font: '25px Palatino', fill: '#000000' }); 
        Health.anchor.setTo(0.5, 0.5);
        Score = game.add.text(1180, 40, 'Score:'+game.global.score, { font: '25px Palatino', fill: '#000000' }); 
        Score.anchor.setTo(0.5, 0.5);
        Ultimate = game.add.text(1110, 80, 'Ultimate skill power(key U):'+ultimateSkills+'/15', { font: '25px Palatino', fill: '#000000' }); 
        Ultimate.anchor.setTo(0.5, 0.5);

        //暂停游戏
        Pause = game.add.text(60, 30, 'Pause', { font: '25px Palatino', fill: '#000000' }); 
        Pause.anchor.setTo(0.5, 0.5);
        Pause.inputEnabled = true;
        Pause.events.onInputUp.add(this.pauseGame);
        pauseLabel = game.add.text(game.width/2, game.height/2, '', { font: '25px Palatino', fill: '#000000' }); 
        pauseLabel.anchor.setTo(0.5, 0.5);
        game.input.onDown.add(this.unpause, self);


        //飞机
        this.player = game.add.sprite(game.width/2, 500, 'player');
        this.player.scale.setTo(2,2);
        this.player.animations.add('toright', [3,2], 8, true);
        this.player.animations.add('toleft', [2,3], 8, true);
        game.physics.arcade.enable(this.player);
        this.player.body.collideWorldBounds = true;
        
        //子弹
        this.bullets = game.add.weapon(120, 'bullet1');
        this.bullets.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
	    this.bullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets.bulletSpeed = 400;
        this.bullets.fireRate = 80;
        this.bullets.bulletAngleOffset = 90;
        this.bullets.trackSprite(this.player, 60, 0);
        this.bullets.multiFire = true;
        playerBulletsPosition = [
                { x: -50, y: 0 },
                { x: 0, y: 0 },
                { x: 50, y: 0 },
        ];
    
        //处理双人模式
        if(game.global.player==2){
            this.player2 = game.add.sprite(game.width/4, 500, 'player');
            this.player2.scale.setTo(2,2);
            this.player2.animations.add('toright', [3,2], 8, true);
            this.player2.animations.add('toleft', [2,3], 8, true);
            game.physics.arcade.enable(this.player2);
            this.player2.body.collideWorldBounds = true;
            
            //子弹
            this.bullets2 = game.add.weapon(120, 'bullet1');
            this.bullets2.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
            this.bullets2.enableBody = true;
            this.bullets2.physicsBodyType = Phaser.Physics.ARCADE;
            this.bullets2.bulletSpeed = 400;
            this.bullets2.fireRate = 80;
            this.bullets2.bulletAngleOffset = 90;
            this.bullets2.trackSprite(this.player2, 60, 0);
            this.bullets2.multiFire = true;
        }


        //按键
        this.cursor = game.input.keyboard.createCursorKeys();
        this.fireKey = game.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);
        this.skillsKey = game.input.keyboard.addKey(Phaser.KeyCode.U);
        this.AKey = game.input.keyboard.addKey(Phaser.KeyCode.A);
        this.SKey = game.input.keyboard.addKey(Phaser.KeyCode.S);
        this.DKey = game.input.keyboard.addKey(Phaser.KeyCode.D);
        this.WKey = game.input.keyboard.addKey(Phaser.KeyCode.W);
        this.FKey = game.input.keyboard.addKey(Phaser.KeyCode.F);

        //敌机
        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        this.enemiesBullets = [];
        this.timer1 = game.time.events.repeat(2000, 600, this.addEnemy, this);
        
        //子弹击中粒子特效
        // this.hitEmitter = game.add.emitter(0, 0, 15);
        // this.hitEmitter.makeParticles('pixel');
        // this.hitEmitter.setYSpeed(-150, 150); 
        // this.hitEmitter.setXSpeed(-150, 150);
        // this.hitEmitter.setScale(2, 0, 2, 0, 800);
        // this.hitEmitter .gravity = 0;

         //boss击毁特效
         this.bossEmitter = game.add.emitter(0, 0, 100);
         this.bossEmitter.makeParticles('pixel');
         this.bossEmitter.setYSpeed(-150, 150); 
         this.bossEmitter.setXSpeed(-150, 150);
         this.bossEmitter.setScale(5, 0, 5, 0, 800);
         this.bossEmitter .gravity = 0;
        
        //ultimate skills
        this.timer2 = game.time.events.repeat(Phaser.Timer.SECOND*1, 600, this.skillPower, this);
        skillBulletsPosition = [
                { x: -250, y: 0 },
                { x: -200, y: 0 },
                { x: -150, y: 0 },
                { x: -100, y: 0 },
                { x: -50, y: 0 },
                { x: 0, y: 0 },
                { x: 50, y: 0 },
                { x: 100, y: 0 },
                { x: 150, y: 0 },
                { x: 200, y: 0 },
                { x: 250, y: 0 },
        ];
            
        //boss
        this.timer3= game.time.events.add(Phaser.Timer.SECOND*60, this.addBoss, this);
        this.timer4= game.time.events.repeat(Phaser.Timer.QUARTER*1, 600, this.bossFire, this);
        bossBulletsPosition = [
                { x: -60, y: 160 },
                { x: -20, y: 160 },
                { x: 20, y: 160 },
                { x: 60, y: 160 },
        ];
        
        //enhanced items
        if(game.global.player==1){
        this.timer5= game.time.events.add(Phaser.Timer.SECOND*26, this.addItem, this);
        this.timer6= game.time.events.add(Phaser.Timer.SECOND*30, this.distroyItem, this);
        this.timer6= game.time.events.add(Phaser.Timer.SECOND*36, this.endenhance, this);

        this.bulletsAuto = game.add.weapon(120, 'bullet3');
        this.bulletsAuto.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
	    this.bulletsAuto.enableBody = true;
        this.bulletsAuto.physicsBodyType = Phaser.Physics.ARCADE;
        this.bulletsAuto.bulletSpeed = 1200;
        this.bulletsAuto.fireRate = 80;
        this.bulletsAuto.bulletAngleOffset = 90;
        this.bulletsAuto.trackSprite(this.player, 60, 0);
        this.bulletsAuto.multiFire = true;
        autoBulletsPosition = [
            { x: -75, y: 0 },
            { x: -25, y: 0 },
            { x: 25, y: 0 },
            { x: 75, y: 0 },
        ];

        // this.helper = game.add.sprite(game.width*3/4, 500, 'helper');
        // this.helper.scale.setTo(0.16,0.16);
        // game.physics.arcade.enable(this.helper);

        this.bulletsHelper = game.add.weapon(120, 'bullet1');
        this.bulletsHelper.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
	    this.bulletsHelper.enableBody = true;
        this.bulletsHelper.physicsBodyType = Phaser.Physics.ARCADE;
        this.bulletsHelper.bulletSpeed = 400;
        this.bulletsHelper.fireRate = 500;
        this.bulletsHelper.bulletAngleOffset = 90;
        this.bulletsHelper.fireAngle = 225;
        // this.bulletsHelper.trackSprite(this.helper, 60, 0);
        this.bulletsHelper.multiFire = true;
        helperBulletsPosition = [
                { x: -50, y: 0 },
                { x: 0, y: 0 },
                { x: 50, y: 0 },
        ];    
        }

        //sound effect
        this.shootSound = game.add.audio('shoot');
        this.bgmSound = game.add.audio('bgm');
        this.bgmSound.loop = true;
        this.bgmSound.play();

        //volume control
        this.bounds= new Phaser.Rectangle(1030,100,194,44);
        var graphics = game.add.graphics(this.bounds.x, this.bounds.y+11);
        graphics.beginFill(0xFFF0F5);
        graphics.drawRect(0, 0, this.bounds.width, this.bounds.height/2);

        this.slidebar = game.add.sprite(1180, 100, 'slider');
        this.slidebar.inputEnabled = true;
        this.slidebar.input.enableDrag(false,false,false,255,this.bounds);
        this.slidebar.input.allowVerticalDrag = false;
        this.slidebar.events.onDragStop.add(this.setVolume, this);
    },
    pauseGame: function(){
        game.paused = true;
        Pause.setText('');
        pauseLabel.setText('click to continue');
    },
    unpause: function(){
        if(game.paused){
        game.paused = false;
        Pause.setText('Pause');
        pauseLabel.setText('');
        }
    },
    skillPower: function(){
        if(ultimateSkills<15) ultimateSkills++;
    },
    setVolume: function(){
        volumeNum = (this.slidebar.x - this.bounds.x)/150;
        if(volumeNum>1) volumeNum = 1;
        this.shootSound.volume = volumeNum;
        this.bgmSound.volume = volumeNum;
    },
    bossFire: function() {
        //boss攻击间隔
        if(bossAttack && bossCount<24){
        bossCount+=3;
        }
        else if(bossAttack && bossCount==24){
        bossAttack = false;
        }
        else if(!bossAttack && bossCount>0){
        bossCount-=2;
        }
        else if(!bossAttack && bossCount==0){
        bossAttack = true;
        }
    },
    addItem: function(){
        itemdisplay = 1;
        this.item = game.add.sprite(game.width/2, 550, 'item');//bug?
        this.item.scale.setTo(0.5,0.5);
        game.physics.arcade.enable(this.item);
    },
    distroyItem: function(){
        if(itemdisplay){
        itemdisplay = 0;
        this.item.kill();
        }
    },
    endenhance: function(){
        if(enhanceMode){
        enhanceMode = 0;
        this.helper.kill();
        }
    },
    addEnemy: function(){
        //敌机最少x架
        if(this.enemiesBullets.length > 8){
                timer1.delay = 1000;
        }
        if(this.enemiesBullets.length < 5){
        // 添加敌机
            var enemy = this.add.sprite(game.world.randomX, -30, 'enemy');
            game.physics.arcade.enable(enemy);
            enemy.scale.setTo(0.12,0.12);
            //level设置
            if(game.global.level==1)  enemy.body.velocity.setTo(100, 100);
            else if(game.global.level==2)  enemy.body.velocity.setTo(130, 130);
            else enemy.body.velocity.setTo(160, 160);
            enemy.body.collideWorldBounds = true;
            enemy.body.bounce.set(1);
            enemy.authcode = enemyIndex;
            this.enemies.add(enemy);
            
            var newbullet = game.add.weapon(4, 'bullet2');
            newbullet.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
            newbullet.enableBody = true;
            newbullet.physicsBodyType = Phaser.Physics.ARCADE;
            newbullet.bulletAngleOffset = 90;
            //level设置
            if(game.global.level==1) newbullet.bulletSpeed = -400;
            else if(game.global.level==2) newbullet.bulletSpeed = -500;
            else newbullet.bulletSpeed = -600;
            newbullet.trackSprite(enemy, 0, 0);
            newbullet.fireRate = 1200;
            newbullet.autofire = true;
            newbullet.authcode = enemyIndex;
            this.enemiesBullets.push(newbullet);
        }
        enemyIndex++;
},
    addBoss: function(){
        this.boss = this.add.sprite(520, 0, 'boss');
        this.boss.anchor.setTo(0.5, 0);
        this.boss.scale.setTo(0.7, 0.7);
        this.boss.life = 100;
        game.physics.arcade.enable(this.boss);
        //boss独特移动方式
        game.add.tween(this.boss).to({x: this.boss.x+300}, 2000).yoyo(true).loop(true).start();

        bossAttack = true;
        this.bossBullets = game.add.weapon(4*20, 'bullet2');
        this.bossBullets.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bossBullets.enableBody = true;
        this.bossBullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bossBullets.bulletAngleOffset = 90;
        this.bossBullets.bulletSpeed = -400;
        this.bossBullets.trackSprite(this.boss, 0, 0);
        this.bossBullets.fireRate = 100;
        this.bossBullets.multiFire = true;
        
        bossdisplay = true;
        bossCount = 0;
        bossKill = 0;
    },
    
    update: function() {

        //UI更新
        Health.setText('Health:'+game.global.health);
        Score.setText('Score:'+game.global.score);
        Ultimate.setText('Ultimate skill power:'+ultimateSkills+'/15');

        //道具
        if(itemdisplay){
        game.physics.arcade.collide(this.player, this.item, this.getItem, null, this);
        }
        if(enhanceMode){
        game.physics.arcade.overlap(this.enemies, this.bulletsAuto.bullets, this.HitEnemy, null, this);
        game.physics.arcade.overlap(this.enemies, this.bulletsHelper.bullets, this.HitEnemy, null, this);
        this.bulletsHelper.fireMany(helperBulletsPosition);
        }

        //击中敌机
        game.physics.arcade.overlap(this.enemies, this.bullets.bullets, this.HitEnemy, null, this);
        if(game.global.player==2) {game.physics.arcade.overlap(this.enemies, this.bullets2.bullets, this.HitEnemy, null, this);}
        if(bossdisplay){
        game.physics.arcade.overlap(this.boss, this.bullets.bullets, this.HitBoss, null, this);
        if(game.global.player==2) {game.physics.arcade.overlap(this.boss, this.bullets2.bullets, this.HitBoss, null, this);}
        }
        //相撞
        game.physics.arcade.collide(this.enemies, this.player, this.enemyHitPlayer, null, this);
        if(game.global.player==2) {game.physics.arcade.collide(this.enemies, this.player2, this.enemyHitPlayer, null, this);}
        if(bossdisplay){
        game.physics.arcade.overlap(this.boss, this.player, this.bossHitPlayer, null, this);
        if(game.global.player==2) {game.physics.arcade.overlap(this.boss, this.player2, this.bossHitPlayer, null, this);}
        }
	    //子弹击中飞机
	    for(var j=0; j<this.enemiesBullets.length; j++){
        game.physics.arcade.overlap(this.enemiesBullets[j].bullets, this.player, this.enemyBulletsHitPlayer, null, this);
        if(game.global.player==2) {game.physics.arcade.overlap(this.enemiesBullets[j].bullets, this.player2, this.enemyBulletsHitPlayer, null, this);}
	    }
	    if(bossdisplay){
        game.physics.arcade.overlap(this.bossBullets.bullets, this.player, this.bossBulletsHitPlayer, null, this);
        if(game.global.player==2) {game.physics.arcade.overlap(this.bossBullets.bullets, this.player2, this.bossBulletsHitPlayer, null, this);}
	    }
        
        if(bossdisplay){
        //boss攻击
        if(bossAttack && this.boss.life >= 0){
                this.bossBullets.fireMany(bossBulletsPosition);
        }
        //boss消失，玩家胜利
        if(bossdisplay && this.boss.life < 0){
                bossdisplay = false;
                bossAttack = false;
                var BGM = this.bgmSound;
                game.time.events.add(2000, function(){
                        BGM.stop();
                        game.input.keyboard.enabled = false;
                        game.state.start('end');
                });               
        }
        }
        //玩家失败
        if(game.global.health < 0){
                bossdisplay = false;
                bossAttack = false;
                this.bgmSound.stop();
                game.input.keyboard.enabled = false;
                game.state.start('end');
        }
        //移动地图
        this.map.tilePosition.y += 2;
        this.movePlayer();
    },
    getItem: function(player, item){
        itemdisplay = 0;
        item.kill();
        enhanceMode = 1;
        this.helper = game.add.sprite(game.width*3/4, 500, 'helper');
        this.helper.scale.setTo(0.16,0.16);
        game.physics.arcade.enable(this.helper);
        this.bulletsHelper.trackSprite(this.helper, 60, 0);
    },
    HitEnemy: function(enemy, bullet) {
        bullet.kill();

        var hitEmitter = game.add.emitter(0, 0, 15);
        hitEmitter.makeParticles('pixel');
        hitEmitter.setYSpeed(-150, 150); 
        hitEmitter.setXSpeed(-150, 150);
        hitEmitter.setScale(2, 0, 2, 0, 800);
        hitEmitter.gravity = 0;
        hitEmitter.x = enemy.x+50;
        hitEmitter.y = enemy.y+50;
        hitEmitter.start(true, 800, null, 15);

        var killindex = enemy.authcode;;
        enemy.kill();
        for(var i=0; i<this.enemiesBullets.length; i++){
            if(killindex==this.enemiesBullets[i].authcode){
                //level设置
                if(game.global.level==1) game.global.score += 10;
                else if(game.global.level==2) game.global.score += 20;
                else game.global.score += 30;
                this.enemiesBullets[i].destroy();
                this.enemiesBullets.splice(i, 1);
                break;
            }
        }
    },
    HitBoss: function(boss, bullet) {
        bullet.kill();
        if(!bossKill){
        this.boss.life -= 1;
        if(this.boss.life < 0){
                game.global.score += 100;
                this.bossEmitter.x = this.boss.x;
                this.bossEmitter.y = this.boss.y+80;
                this.bossEmitter.start(true, 4000, null, 15);
                this.boss.kill();
                this.bossBullets.destroy();
                bossKill = 1;
        } 
} 
    },
    enemyHitPlayer: function(player, enemy){
        game.global.health--;
        var killindex = enemy.authcode;;
        enemy.kill();
        for(var i=0; i<this.enemiesBullets.length; i++){
            if(killindex==this.enemiesBullets[i].authcode){
                this.enemiesBullets[i].destroy();
                this.enemiesBullets.splice(i, 1);
                break;
            }
        }

        var hitEmitter = game.add.emitter(0, 0, 15);
        hitEmitter.makeParticles('pixel');
        hitEmitter.setYSpeed(-150, 150); 
        hitEmitter.setXSpeed(-150, 150);
        hitEmitter.setScale(2, 0, 2, 0, 800);
        hitEmitter.gravity = 0;
        hitEmitter.x = player.x+50;
        hitEmitter.y = player.y+50;
        hitEmitter.start(true, 800, null, 15);
    },
    bossHitPlayer: function(boss, player){
        game.global.health--;

        var hitEmitter = game.add.emitter(0, 0, 15);
        hitEmitter.makeParticles('pixel');
        hitEmitter.setYSpeed(-150, 150); 
        hitEmitter.setXSpeed(-150, 150);
        hitEmitter.setScale(2, 0, 2, 0, 800);
        hitEmitter.gravity = 0;
        hitEmitter.x = player.x+50;
        hitEmitter.y = player.y+50;
        hitEmitter.start(true, 800, null, 15);
    },
    enemyBulletsHitPlayer: function(player, enemyBullet){
        enemyBullet.kill();
        game.global.health--;

        var hitEmitter = game.add.emitter(0, 0, 15);
        hitEmitter.makeParticles('pixel');
        hitEmitter.setYSpeed(-150, 150); 
        hitEmitter.setXSpeed(-150, 150);
        hitEmitter.setScale(2, 0, 2, 0, 800);
        hitEmitter.gravity = 0;
        hitEmitter.x = player.x+50;
        hitEmitter.y = player.y+50;
        hitEmitter.start(true, 800, null, 15);
    },
    bossBulletsHitPlayer: function(player, bossBullet){
        bossBullet.kill();
        game.global.health--;

        var hitEmitter = game.add.emitter(0, 0, 15);
        hitEmitter.makeParticles('pixel');
        hitEmitter.setYSpeed(-150, 150); 
        hitEmitter.setXSpeed(-150, 150);
        hitEmitter.setScale(2, 0, 2, 0, 800);
        hitEmitter.gravity = 0;
        hitEmitter.x = player.x+50;
        hitEmitter.y = player.y+50;
        hitEmitter.start(true, 800, null, 15);
    },
    movePlayer: function() {
        if(this.fireKey.isDown && game.global.health >= 0){
        this.shootSound.play();
        if(enhanceMode){
            var shootFlag = 0;
            this.enemies.forEachAlive(function(enemy){
                if(enemy.y < this.player.y && shootFlag==0){
                shootFlag = 1;
                var dx = enemy.x-this.player.x;
                var dy = enemy.y-this.player.y;
                this.bulletsAuto.fireAngle = Math.atan2(dy, dx)*180/Math.PI;
                this.bulletsAuto.fireMany(autoBulletsPosition);
                }
              }, this);
        
        }else{
        this.bullets.fireMany(playerBulletsPosition);
        }
        }
        //使用技能
        else if(this.skillsKey.isDown && ultimateSkills==15 && game.global.health >= 0){
        ultimateSkills = 0;
        this.shootSound.play();
        this.bullets.fireMany(skillBulletsPosition);   
        } 

        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -200;
            this.player.animations.play('toleft');
        }
        else if (this.cursor.right.isDown) { 
            this.player.body.velocity.x = 200;
            this.player.animations.play('toright');
        }    

        // If the up arrow key is pressed, And the player is on the ground.
        else if (this.cursor.up.isDown) {         
                this.player.body.velocity.y = -200;
        }
        else if (this.cursor.down.isDown) {         
                this.player.body.velocity.y = 200;
        }
        // If neither the right or left arrow key is pressed
        else {
                this.player.body.velocity.x = 0;
                this.player.body.velocity.y = 0;
            
                this.player.frame = 4;
                this.player.animations.stop();
            }

        if(game.global.player==2){
            if(this.FKey.isDown && game.global.health >= 0){
                this.shootSound.play();
                this.bullets2.fireMany(playerBulletsPosition);
                }
            
                if (this.AKey.isDown) {
                    this.player2.body.velocity.x = -200;
                    this.player2.animations.play('toleft');
                }
                else if (this.DKey.isDown) { 
                    this.player2.body.velocity.x = 200;
                    this.player2.animations.play('toright');
                }    
        
                // If the up arrow key is pressed, And the player is on the ground.
                else if (this.WKey.isDown) {         
                        this.player2.body.velocity.y = -200;
                }
                else if (this.SKey.isDown) {         
                        this.player2.body.velocity.y = 200;
                }
                // If neither the right or left arrow key is pressed
                else {
                        this.player2.body.velocity.x = 0;
                        this.player2.body.velocity.y = 0;
                    
                        this.player2.frame = 4;
                        this.player2.animations.stop();
                    }

        }
    },

    
};