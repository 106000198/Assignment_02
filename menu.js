
var menuState = { 
    create: function() {
    // Add a background image 
    game.add.image(0, 0, 'background');
    // Display the name of the game
    var nameLabel = game.add.text(game.width/2, 200, 'Raiden', { font: '80px Palatino', fill: '#ffffff' }); 
    nameLabel.anchor.setTo(0.5, 0.5);
    // Show the level at the center of the screen
    this.level1Label = game.add.text(game.width/2, 600, 'Level 1 (press key 1)', { font: '25px Palatino', fill: '#ffd700' }); 
    this.level1Label.anchor.setTo(0.5, 0.5);
    this.level2Label = game.add.text(game.width/2, 640, 'Level 2 (press key 2)', { font: '25px Palatino', fill: '#ffffff' }); 
    this.level2Label.anchor.setTo(0.5, 0.5);
    this.level3Label = game.add.text(game.width/2, 680, 'Level 3 (press key 3)', { font: '25px Palatino', fill: '#ffffff' }); 
    this.level3Label.anchor.setTo(0.5, 0.5);
    this.key1 = game.input.keyboard.addKey(Phaser.Keyboard.ONE);
    this.key2 = game.input.keyboard.addKey(Phaser.Keyboard.TWO);
    this.key3 = game.input.keyboard.addKey(Phaser.Keyboard.THREE);
    //multi-player
    this.player1Label = game.add.text(game.width/2, 520, 'Player 1 (press key S)', { font: '25px Palatino', fill: '#ffd700' }); 
    this.player1Label.anchor.setTo(0.5, 0.5);
    this.player2Label = game.add.text(game.width/2, 560, 'Player 2 (press key D)', { font: '25px Palatino', fill: '#ffffff' }); 
    this.player2Label.anchor.setTo(0.5, 0.5);
    this.keySingle = game.input.keyboard.addKey(Phaser.Keyboard.S);
    this.keyDouble = game.input.keyboard.addKey(Phaser.Keyboard.D);
    //默认level 1/player 1
    game.global.level = 1;
    game.global.player = 1;
    // Explain how to start the game
    var startLabel = game.add.text(game.width/2, game.height-80, 'press the enter key to start', { font: '25px Palatino', fill: '#ffffff' }); 
    startLabel.anchor.setTo(0.5, 0.5);
    // Create a new Phaser keyboard variable: the up arrow key
    // When pressed, call the 'start'
    var upKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
    upKey.onDown.add(this.start, this); 
    },
    update: function() {
        if(this.keySingle.isDown){
            game.global.player = 1;
            this.player1Label.addColor('#ffd700',0);
            this.player2Label.addColor('#ffffff',0);
        }
        else if(this.keyDouble.isDown){
            game.global.player = 2;
            this.player1Label.addColor('#ffffff',0);
            this.player2Label.addColor('#ffd700',0);
        }

        if(this.key1.isDown){
            game.global.level = 1;
            this.level1Label.addColor('#ffd700',0);
            this.level2Label.addColor('#ffffff',0);
            this.level3Label.addColor('#ffffff',0);
        }
        else if(this.key2.isDown){
            game.global.level = 2;
            this.level1Label.addColor('#ffffff',0);
            this.level2Label.addColor('#ffd700',0);
            this.level3Label.addColor('#ffffff',0);
        }
        else if(this.key3.isDown){
            game.global.level = 3;
            this.level1Label.addColor('#ffffff',0);
            this.level2Label.addColor('#ffffff',0);
            this.level3Label.addColor('#ffd700',0);
        }
    },
    start: function() {
    game.state.start('play');
    }, 
};