
var game = new Phaser.Game(1280, 860, Phaser.AUTO, 'canvas'); 
// Define our global variable
game.global = { score: 0, health: 20, level: 1, player: 1};
// Add all the states
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('end', endState);
game.state.add('quit', quitState);
// Start the 'boot' state
game.state.start('boot');