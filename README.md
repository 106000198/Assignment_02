# Assignment_02
1.Jucify mechanisms
    在開始界面選擇遊戲難度，有三個level，不同難度之間的區別是敵機的移動速度、敵機投放的子彈的移動速度和擊毀一架敵機的得分。
    遊戲介面右上角顯示ultimate skill power，當成為15/15時，按U鍵使用ultimate skill，會一次發出較多子彈。
    
2.Bonus
(1) Multi-player off-line模式，在開始界面可以選擇雙人模式。雙人模式下，一個玩家用上下左右鍵移動，空格鍵攻擊，U鍵發動ultimate skill，另一個玩家用WSAD鍵移動，F鍵攻擊，不能使用ultimate skill；此模式下沒有enhanced items。
(2) Enhanced items功能，只有單人模式有此功能。遊戲開始一段時間後，出現劍的樣子的道具，操控小飛機拿到道具，進入增強模式。玩家此時發射出的子彈會自動瞄準；子彈顏色變為藍色區別於普通模式的黃色，子彈的移動速度變快很多；頁面上出現一個助手飛機，會自動攻擊。
(3) 遊戲最後出現Boss，Boss在頁面頂端左右移動，每隔一段時間後密集發出子彈，玩家擊敗Boss遊戲結束。

3.其他部分說明
Complete game process：開始菜單界面選擇單雙人模式和遊戲level，enter鍵進入遊戲；進行遊戲；遊戲結束界面，點擊quit進入已退出遊戲頁面，點擊restart進入開始菜單界面。
Animations：玩家左右移動時飛機有動畫。
Particle Systems：玩家或敵機被子彈擊中時有particle爆炸特效。
Sound effects：兩種音效，子彈射擊聲和背景音樂，控制音量的拖動條在遊戲介面右上角。
UI：Player health, Score, Ultimate skill power, volume control 在遊戲界面右上角，Game pause在遊戲界面左上角。點擊左上角pause，遊戲暫停，點擊界面上任意位置，遊戲繼續。
Leaderboard：遊戲結束後的界面上有輸入player name的input區域，點擊confirm後界面右側出現leaderboard，顯示前十名玩家的成績。




