var sameName = 0;
var leaderboard

var endState = { 
    create: function() {
    // Add a background image 
    game.add.image(0, 0, 'background');
    var scoreLabel = game.add.text(game.width/4, 200, 'Score:'+game.global.score, { font: '60px Palatino', fill: '#ffffff' }); 
    scoreLabel.anchor.setTo(0.5, 0.5);
    var quitLabel = game.add.text(game.width/4, 600, 'quit', { font: '45px Palatino', fill: '#ffffff' }); 
    quitLabel.anchor.setTo(0.5, 0.5);
    var restartLabel = game.add.text(game.width/4, 700, 'restart', { font: '45px Palatino', fill: '#ffffff' }); 
    restartLabel.anchor.setTo(0.5, 0.5);
    // Create a new Phaser keyboard variable: the up arrow key
    // When pressed, call the 'start'
    quitLabel.inputEnabled = true;
    quitLabel.events.onInputUp.add(this.quit);
    restartLabel.inputEnabled = true;
    restartLabel.events.onInputUp.add(this.restart);

    //leaderboard
    var playername = document.getElementById('playername');
    playername.innerHTML = '<input type="text" id="textname" maxlength="10" placeholder="PlayerName" required><button id="btnname">Confirm</button>'
    leaderboard = document.getElementById('leaderboard');
    boardName = document.getElementById('showname');
    boardScore = document.getElementById('showscore');


    var btnname = document.getElementById('btnname');
    var textname = document.getElementById('textname');
    btnname.addEventListener('click', function (event) {
            
            if (textname.value != '')
            {
                playername.innerHTML = '';

                var leaderLabel = game.add.text(330, 290, 'Have a look if you are on the leaderboard!', { font: '35px Palatino', fill: '#ffffff' }); 
                leaderLabel.anchor.setTo(0.5, 0.5);

                sameName = 0;
                var noderef = firebase.database().ref('players');
                noderef.once('value').then(function(snapshot){

                    snapshot.forEach(function(childSnapshot) {
        
                                if(childSnapshot.val().name==textname.value){
                                sameName = 1;
                                if(game.global.score > childSnapshot.val().score){
                                    childSnapshot.ref.set({
                                        name: textname.value,
                                        score: game.global.score,
                                      });  
                                }
                                }    
                                
                            })
                    
                      }).then(function(){
                        if(!sameName){
                            noderef.push().set({
                                name: textname.value,
                                score: game.global.score,
                              });  
                        }

                        leaderboard.style.width = '260px';
                        var total_list1 = [];
                        var total_list2 = [];
                        var Leaderref = firebase.database().ref('players');
                        Leaderref.orderByChild("score").limitToLast(10).once('value', function (snap) {
                        snap.forEach(function (item) {
                            total_list1.unshift('<p>' + item.val().name + '</p>'); 
                            total_list2.unshift('<p>' + item.val().score + '</p>');
                            boardName.innerHTML = total_list1.join(''); 
                            boardScore.innerHTML = total_list2.join(''); 
                        })
                        });

                    });
                
            }
            
            
    });
    

    },
    quit: function() {
    playername.innerHTML = '';
    boardName.innerHTML = '';
    boardScore.innerHTML = '';
    leaderboard.style.width = '0px';
    game.state.start('quit');
    },
    restart: function() {
    playername.innerHTML = '';
    boardName.innerHTML = '';
    boardScore.innerHTML = '';
    leaderboard.style.width = '0px';
    game.input.keyboard.enabled = true;
    game.state.start('menu');
    }, 
};