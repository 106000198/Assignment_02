var quitState = { 
    create: function() {
    // Add a background image 
    game.add.image(0, 0, 'background');
    var wordLabel = game.add.text(game.width/2, game.height/2, "you've quit the game", { font: '60px Palatino', fill: '#ffffff' }); 
    wordLabel.anchor.setTo(0.5, 0.5);
    }
};
