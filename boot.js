
var bootState = { 
    preload: function () {
    // Load the progress bar image.
    game.load.image('progressBar', 'assets/progressBar.png');
    game.load.image('background', 'assets/background.jpg'); 
    },
    create: function() {
    // Set some game settings. 
    game.stage.backgroundColor = '#009900'; 
    game.physics.startSystem(Phaser.Physics.ARCADE); 
    game.renderer.renderSession.roundPixels = true;
    // Start the load state.
    game.state.start('load');
    }
 };