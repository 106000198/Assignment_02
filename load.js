
var loadState = { 
    preload: function () {
    game.add.image(0, 0, 'background');
    // Add a 'loading...' label on the screen
    var loadingLabel = game.add.text(game.width/2, 300, 'loading...', { font: '30px Arial', fill: '#ffffff' }); 
    loadingLabel.anchor.setTo(0.5, 0.5);
    // Display the progress bar
    var progressBar = game.add.sprite(game.width/2, 350, 'progressBar'); 
    progressBar.anchor.setTo(0.5, 0.5); 
    game.load.setPreloadSprite(progressBar);
    // Load all game assets
    game.load.image('bullet1', 'assets/bullet1.png'); 
    game.load.image('bullet2', 'assets/bullet2.png'); 
    game.load.image('enemy', 'assets/enemy.png');
    game.load.image('boss', 'assets/boss.png'); 
    game.load.image('map', 'assets/map.png'); 
    game.load.image('pixel', 'assets/pixel.png'); 
    game.load.spritesheet('player', 'assets/spritesheet.png', 47, 54);
    game.load.spritesheet('explosion', 'assets/explosion.png', 50, 50);
    
    game.load.audio('shoot', 'assets/shoot.wav');
    game.load.audio('bgm', 'assets/bgm.wav');
    game.load.image('slider', 'assets/slider.png'); 

    game.load.image('bullet3', 'assets/bullet3.png'); 
    game.load.image('item', 'assets/item.png'); 
    game.load.image('helper', 'assets/helper.png'); 
    },
    create: function() {
    game.state.start('menu');//
    } 
};